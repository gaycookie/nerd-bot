// Generated by CoffeeScript 2.5.1
(function() {
  var db, knex;

  knex = require('knex');

  db = knex({
    client: 'sqlite3',
    useNullAsDefault: true,
    connection: {
      filename: 'database.sqlite'
    }
  });

  db.schema.hasTable('users').then(function(exist) {
    if (exist) {
      return;
    }
    return db.schema.createTable('users', function(table) {
      table.string('user_id').unique().index().notNullable();
      table.integer('total_votes').defaultTo(0);
      table.integer('month_votes').defaultTo(0);
      table.boolean('fish_reminder').defaultTo(0);
      table.boolean('trivia_reminder').defaultTo(0);
      table.boolean('unbox_reminder').defaultTo(0);
      return table.boolean('opped_out').defaultTo(0);
    });
  });

  db.schema.hasTable('wallets').then(function(exist) {
    if (exist) {
      return;
    }
    return db.schema.createTable('wallets', function(table) {
      table.string('user_id').unique().index().notNullable();
      table.integer('vp').defaultTo(0); //# Vote Points
      return table.integer('ink').defaultTo(100); //# International Kredit
    });
  });

  db.schema.hasTable('month_user').then(function(exist) {
    if (exist) {
      return;
    }
    return db.schema.createTable('month_user', function(table) {
      table.string('user_id').index().notNullable();
      table.integer('month').notNullable();
      table.integer('year').notNullable();
      return table.unique(['month', 'year']);
    });
  });

  module.exports = db;

}).call(this);
