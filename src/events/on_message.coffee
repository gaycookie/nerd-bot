Discord = require 'discord.js'

trivia = new Set
unboxed = new Set
fished = new Set

module.exports = 
  name: 'On Message',
  event: 'message',
  enabled: true,
  function: (bot, msg) -> 

    # If the bots detect a message from a bot, it will silently ignores it.
    if msg.author.bot 
      return true

    # If it's not a Guild Text channel, it will ignore
    if not msg.channel.type == "text"
      console.log "This was not a server channel"
      return true

    # If it has not the correct permissions, it will ignore
    if not msg.guild.me.permissionsIn(msg.channel).has "SEND_MESSAGES"
      return true

    # Command stuff comes below!
    prefix_regex = new RegExp "^(<@!?#{bot.user.id}>|#{"-".replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&')})\\s*"
    if not prefix_regex.test msg.content
      return true
    matched_prefix = msg.content.match prefix_regex

    args = msg.content.slice matched_prefix.length
      .split /\s+/
    command_name = args.shift().toLowerCase()
    command = bot.commands.get(command_name) || bot.commands.find (cmd) -> cmd.aliases && cmd.aliases.includes command_name

    if not command
      return true

    # Bot owners avoid permissions checks

    if not [''].includes msg.author.id
      return true
      