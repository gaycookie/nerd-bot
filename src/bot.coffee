fs = require 'fs'
path = require 'path'
Discord = require 'discord.js'

bot = new Discord.Client()
bot.commands = new Discord.Collection()
bot.database = require './database'
bot.user_settings = new Discord.Collection()

bot.on 'ready', () ->
  console.log "Logged in as #{bot.user.tag}"
  bot.fetchApplication().then (app) -> bot.owner = app.owner

  fs.readdir path.join(__dirname, 'events'), {}, (err, files) ->
    if err 
      process.exit err.code
    files.forEach (file) ->
      event = require path.join __dirname, 'events', file
      if event.enabled
        bot.on event.event, (...args) -> event.function bot, ...args

  fs.readdir path.join(__dirname, 'modules'), {}, (err, files) ->
    if err 
      process.exit err.code
    files.forEach (file) ->
      module = require path.join __dirname, 'modules', file
      if module.enabled
        module.function bot

bot.login process.env.TOKEN