default_options = 
  fish_reminder: false,
  unbox_reminder: false,
  trivia_reminder: false,

module.exports = class
  constructor: (@id, @options = default_options) ->

  update: (options) ->
    for prop of options
      if @options.hasOwnProperty prop
        @options[prop] = options[prop]


    console.log @options
