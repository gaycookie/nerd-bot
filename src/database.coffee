knex = require 'knex'
db = knex(
  client: 'sqlite3',
  useNullAsDefault: true,
  connection:
    filename: 'database.sqlite'
)

db.schema.hasTable('users').then((exist) -> 
  if exist
    return

  db.schema.createTable 'users', (table) -> 
    table
      .string 'user_id'
      .unique()
      .index()
      .notNullable()

    table
      .integer 'total_votes'
      .defaultTo 0
    
    table
      .integer 'month_votes'
      .defaultTo 0
    
    table
      .boolean 'fish_reminder'
      .defaultTo 0

    table
      .boolean 'trivia_reminder'
      .defaultTo 0

    table
      .boolean 'unbox_reminder'
      .defaultTo 0

    table
      .boolean 'opped_out'
      .defaultTo 0
)

db.schema.hasTable('wallets').then((exist) -> 
  if exist
    return

  db.schema.createTable 'wallets', (table) -> 
    table
      .string 'user_id'
      .unique()
      .index()
      .notNullable()

    table
      .integer 'vp' ## Vote Points
      .defaultTo 0 

    table
      .integer 'ink' ## International Kredit
      .defaultTo 100 
)

db.schema.hasTable('month_user').then((exist) -> 
  if exist
    return

  db.schema.createTable 'month_user', (table) -> 
    table
      .string 'user_id'
      .index()
      .notNullable()
    
    table
      .integer 'month'
      .notNullable()
    
    table
      .integer 'year'
      .notNullable()
    
    table
      .unique ['month', 'year']
)

module.exports = db