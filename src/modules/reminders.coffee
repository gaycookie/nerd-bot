module.exports = 
  name: 'Reminders',
  module: 'reminders',
  enabled: true,
  function: (bot) -> 

    bot.on 'message', (msg) ->
      if msg.author.bot 
        return true

      if not msg.channel.type == "text"
        return true

      if not msg.guild.me.permissionsIn(msg.channel).has "SEND_MESSAGES"
        return true

      user = bot.user_settings.get(msg.author.id);

      if user and user.options.trivia_reminder
        if ['b!trivia'].includes msg.content
          if not trivia.has msg.author.id
            trivia.add msg.author.id
            setTimeout -> 
              msg.channel.send "You can do another trivia again #{msg.author}!"
              trivia.delete msg.author.id
            , 30 * 60 * 1000

      if user and user.options.unbox_reminder
        if ['b!unbox'].includes msg.content
          if not unboxed.has msg.author.id
            unboxed.add msg.author.id
            setTimeout ->
              msg.channel.send "You can do another unbox again #{msg.author}!"
              unboxed.delete msg.author.id
            , 60 * 60 * 1000

      if user and user.options.fish_reminder
        if ['tfish', 'tfishy', 't!fish', 't!fishy'].includes msg.content
          if not fished.has msg.author.id
            fished.add msg.author.id
            setTimeout ->
              msg.channel.send "You can fish again #{msg.author}!"
              fished.delete msg.author.id
            , 30 * 1000